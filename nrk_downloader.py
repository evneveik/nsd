#!/usr/bin/env python

from lxml import html
from subprocess import call
import argparse
import urllib3
import json

parser = argparse.ArgumentParser(description='Fetch all episodes from a NRK Super TV Series. Does not overwrite existing files.')
parser.add_argument('-u', '--url', metavar='URL', type=str, help='URL to NRK Super TV Series')
seasonurl = parser.parse_args()
#test
#seasonurl = input("URL til en serie:")
http = urllib3.PoolManager()
seasoncontent = http.urlopen('GET', seasonurl).data
seasonhtml = html.fromstring(seasoncontent.decode("utf-8"))
seasons = seasonhtml.cssselect('.seasons-container .season-episodes')

for season in seasons:
    jsonurl = ("http://tv.nrk.no"+season.get("data-url"))
    jsoncontent = http.urlopen('GET', jsonurl).data
    jsonfeed = json.loads(jsoncontent.decode('utf-8'))
    for obj in jsonfeed["Data"]:
        url = ("http://tv.nrk.no"+obj["Url"])
        title = obj["Title"]
        season = obj["SeasonDisplayTitle"]
        episode = obj["EpisodeNumberOrDate"]
        call(["/usr/bin/youtube-dl","--recode-video","mkv",url])


