#!/usr/bin/env python

import re
import os
import io
import sys
import urllib2
import datetime
import getopt
import subprocess
from BeautifulSoup import BeautifulSoup
# from lxml import html

# Possible to include absolute path if needed:
tool = 'ffmpeg'
# tool = 'avconv'


def usage():
    print 'nrk_download.py -u <url>'


def abort(message):
    print "Error!"
    print message
    sys.exit(2)


def xml2srt(text=''):
    soup = BeautifulSoup(text)
    result = u''
    zerotime = datetime.datetime.strptime("0", "%H")   # For simple converting to timedelta

    for i, p in enumerate(soup('p'), start=1):
        begin = datetime.datetime.strptime(p['begin'], '%H:%M:%S.%f')
        end = begin + (datetime.datetime.strptime(p['dur'], '%H:%M:%S.%f') - zerotime)
        section = unicode(i) + '\n'
        section += '%s,%03d' % (begin.strftime('%H:%M:%S'), begin.microsecond/1000)
        section += ' --> '
        section += '%s,%03d' % (end.strftime('%H:%M:%S'), end.microsecond/1000)
        section += '\n'
        pcont = unicode(p)
        pcont = re.sub('<p [^>]*>', '', pcont)
        pcont = re.sub('</p>', '', pcont)
        pcont = pcont.replace('<br />', '\n').replace('<span style="italic"> ', '<i>').replace(' </span>', '</i>')
        section += pcont
        section = section.strip()
        section = re.sub('[\n]{2,}', '\n', section)
        result += section + '\n\n'

    return result

def getvideo(url):
    acodec = 'copy'
    subs_exist = False

    try:
        html_doc = urllib2.urlopen(url).read()
    except urllib2.HTTPError as e:
        abort("Server could not fulfill request.\nHTTP Error code: " + e.code)
    except urllib2.URLError as e:
        abort("Could not reach server.\nReason: " + e.reason)

    soup = BeautifulSoup(html_doc, convertEntities=BeautifulSoup.HTML_ENTITIES)
    title_meta = soup.find("meta", attrs={"name": "title"})
    episode_meta = soup.find("meta", attrs={"name": "episodenumber"})
    if not title_meta:
        abort("Did not recognize HTML structure. Check your url.")

    title = title_meta["content"].strip()
    episode = episode_meta["content"].strip()
    out_filename = re.sub('[/\\\?%\*:|"<>]', '_', title + " " + episode)   # ikke lov: / \ ? % * : | " < >
    p_div = soup.find(id="playerelement")

    if not p_div:
        abort("Did not recognize HTML structure. Check your url.")

    videolink = p_div.get('data-media')

    videolink = re.sub('/\w/', '/i/', videolink)
    videolink = re.sub('manifest.f4m', 'master.m3u8', videolink)

    if p_div.get('data-subtitlesurl'):
        subs_exist = True
        suburl = 'http://' + urllib2.urlparse.urlparse(url).netloc + p_div.get('data-subtitlesurl')

        sub_xml = urllib2.urlopen(suburl).read()
        sub_srt = xml2srt(sub_xml)

        srtfile = io.open(out_filename + '.srt', 'w', encoding="utf-8")
        srtfile.write(sub_srt)
        srtfile.close()

    args = ["-i", videolink]
    if subs_exist:
        args += ["-i", out_filename + ".srt", "-metadata:s:s:0", "language=nor", "-scodec", "copy"]
    args += ["-vcodec", "copy", "-acodec", acodec, out_filename + ".mkv"]

    print "NRK Download.."
    print "Found: " + title + " " + episode
    if subs_exist:
        print "Including subtitles"
    if acodec != "copy":
        print "Transcoding audio to ", acodec
    print "Saving as: ", out_filename + ".mkv"
    print "\n"

    subprocess.call([tool] + args)

    if subs_exist:
        os.remove(out_filename + ".srt")


def main(argv):
    u_arg_passed = False

    try:
        opts, args = getopt.getopt(argv, "hu:a:")

    except getopt.GetoptError:
        usage()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt == '-u':
            url = args
            u_arg_passed = True
            getvideo(url)

    if not u_arg_passed:
        usage()
        sys.exit(2)

if __name__ == "__main__":
    main(sys.argv[1:])
